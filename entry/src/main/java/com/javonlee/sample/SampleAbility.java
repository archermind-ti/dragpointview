package com.javonlee.sample;

import com.alibaba.fastjson.JSONArray;
import com.javonlee.dragpointview.OnPointDragListener;
import com.javonlee.dragpointview.util.LogUtils;
import com.javonlee.dragpointview.view.AbsDragPointView;
import com.javonlee.dragpointview.view.DragPointView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.List;

/**
 * Created by lijinfeng on 2017/7/25.
 */

public class SampleAbility extends AbilitySlice implements OnPointDragListener {

    private List<ConversationEntity> conversationEntities;

    private DragPointView pointView1;
    private DragPointView pointView2;
    private DragPointView pointView3;
    private ListContainer listView;

    private FrameAnimationElement animationDrawable;
    private AnimatorGroup animatorSet;
    private AnimatorGroup objectAnimator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_sample);
        initSlice();
    }

    private void initSlice() {
        initView();
        try {
            initAnim();
        } catch (NotExistException | WrongTypeException | IOException e) {
            LogUtils.e("Sample", "fail to initAnim");
        }
        initSetting();
        initList();
    }

    private void initView() {
        pointView1 = (DragPointView) findComponentById(ResourceTable.Id_drag_point_view1);
        pointView2 = (DragPointView) findComponentById(ResourceTable.Id_drag_point_view2);
        pointView3 = (DragPointView) findComponentById(ResourceTable.Id_drag_point_view3);
        listView = (ListContainer) findComponentById(ResourceTable.Id_list);
    }

    private void initAnim() throws NotExistException, WrongTypeException, IOException {

        animationDrawable = new FrameAnimationElement();

        animationDrawable.addFrame(new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_explode1)), 100);
        animationDrawable.addFrame(new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_explode2)), 100);
        animationDrawable.addFrame(new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_explode3)), 100);
        animationDrawable.addFrame(new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_explode4)), 100);
        animationDrawable.addFrame(new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_explode5)), 100);
        animationDrawable.setOneShot(true);
        animationDrawable.setExitFadeDuration(300);
        animationDrawable.setEnterFadeDuration(100);

        AnimatorProperty objectAnimator1 = new AnimatorProperty(); //"scaleX", 1.f, 0.f
        objectAnimator1.scaleXFrom(1f).scaleX(0f).setDuration(300L);
        AnimatorProperty objectAnimator2 = new AnimatorProperty(); //1.f, 0.f
        objectAnimator2.scaleYFrom(1f).scaleY(0f);
        objectAnimator2.setDuration(300L);
        animatorSet = new AnimatorGroup();
        animatorSet.setDuration(300L);
        animatorSet.runParallel(objectAnimator1, objectAnimator2);

        objectAnimator = new AnimatorGroup(); //"alpha", 1.f, 0.f
        AnimatorProperty objectAnimator3 = new AnimatorProperty();
        objectAnimator3.alphaFrom(1.0f).alpha(0f);
        objectAnimator3.setDuration(300L);
        objectAnimator.setDuration(300L);
        objectAnimator.runParallel(objectAnimator3);
    }

    private void initSetting() {
        pointView1.setRemoveAnim(animationDrawable);
        pointView2.setRemoveAnim(objectAnimator);
        pointView3.setRemoveAnim(animatorSet);
        pointView1.setOnPointDragListener(this);
    }

    private void initList() {
        conversationEntities = JSONArray.parseArray(ConversationEntity.TEST_JSON, ConversationEntity.class);
        listView.setItemProvider(new ItemConversationAdapter(this, conversationEntities));
        updateUnreadCount();
    }

    public void updateUnreadCount() {
        int totalUnreadCount = 0;
        for (ConversationEntity entity : conversationEntities) {
            totalUnreadCount += entity.getMessageNum();
        }
        if (totalUnreadCount > 0) {
            pointView1.setVisibility(Component.VISIBLE);
            if (totalUnreadCount <= 99) {
                pointView1.setText(totalUnreadCount + "");
            } else {
                pointView1.setText("99+");
            }
        } else {
            pointView1.setVisibility(Component.HIDE);
        }
    }

    @Override
    public void onRemoveStart(AbsDragPointView view) {
        for (ConversationEntity entity : conversationEntities) {
            entity.setRead(true);
            entity.setMessageNum(0);
        }
    }

    @Override
    public void onRemoveEnd(AbsDragPointView view) {

    }

    @Override
    public void onRecovery(AbsDragPointView view) {

    }
}
