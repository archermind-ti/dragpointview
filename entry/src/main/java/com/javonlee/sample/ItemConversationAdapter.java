package com.javonlee.sample;

import com.bumptech.glide.Glide;
import com.javonlee.dragpointview.util.LogUtils;
import com.javonlee.dragpointview.view.AbsDragPointView;
import com.javonlee.dragpointview.view.DragPointView;
import com.javonlee.dragpointview.OnPointDragListener;
import ohos.agp.components.*;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ItemConversationAdapter extends BaseItemProvider implements OnPointDragListener {

    private List<ConversationEntity> mObjects = new ArrayList<>();

    private final Context mContext;
    private final LayoutScatter layoutInflater;
    private final FrameAnimationElement animationDrawable;

    public ItemConversationAdapter(Context context, List<ConversationEntity> objects) {
        this.mContext = context;
        this.layoutInflater = LayoutScatter.getInstance(context);
        this.mObjects = objects;

        animationDrawable = new FrameAnimationElement();
        try {
            animationDrawable.addFrame(new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_explode1)), 100);
            animationDrawable.addFrame(new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_explode2)), 100);
            animationDrawable.addFrame(new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_explode3)), 100);
            animationDrawable.addFrame(new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_explode4)), 100);
            animationDrawable.addFrame(new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_explode5)), 100);
        } catch (IOException | NotExistException e) {
            // e.printStackTrace();
            LogUtils.e("Sample", "fail to load animationDrawable");
        }

        animationDrawable.setOneShot(true);
        animationDrawable.setExitFadeDuration(300);
        animationDrawable.setEnterFadeDuration(100);
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override
    public ConversationEntity getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertViewIn, ComponentContainer parent) {
        Component convertView = convertViewIn;
        parent.setClipEnabled(false);
        if (convertView == null) {
            convertView = layoutInflater.parse(ResourceTable.Layout_item_conversation, null, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(ConversationEntity object, ViewHolder holder) {
        Glide.with(mContext).load(object.getAvatar()).into(holder.avatar);
        holder.message.setText(object.getLastMessage());
        holder.name.setText(object.getNickname());
        holder.time.setText(object.getLastTime());
        holder.pointView.setTag(object);
        holder.pointView.setOnPointDragListener(this);
        holder.pointView.setRemoveAnim(animationDrawable);
        if (object.isRead() || object.getMessageNum() <= 0) {
            holder.pointView.setVisibility(Component.HIDE);
        } else {
            holder.pointView.setVisibility(Component.VISIBLE);
            if (object.getMessageNum() <= 99) {
                holder.pointView.setText(object.getMessageNum() + "");
            } else {
                holder.pointView.setText("99");
            }
        }

    }

    @Override
    public void onRemoveStart(AbsDragPointView view) {
    }

    @Override
    public void onRemoveEnd(AbsDragPointView view) {
        ConversationEntity entity =
                (ConversationEntity) view.getTag();
        entity.setMessageNum(0);
        entity.setRead(true);
        ((SampleAbility) mContext).updateUnreadCount();
    }

    @Override
    public void onRecovery(AbsDragPointView view) {

    }

    protected static class ViewHolder {
        private final Image avatar;
        private final Text name;
        private final Text time;
        private final Text message;
        private final DragPointView pointView;

        public ViewHolder(Component view) {
            avatar = (Image) view.findComponentById(ResourceTable.Id_avatar);
            name = (Text) view.findComponentById(ResourceTable.Id_name);
            time = (Text) view.findComponentById(ResourceTable.Id_time);
            message = (Text) view.findComponentById(ResourceTable.Id_message);
            pointView = (DragPointView) view.findComponentById(ResourceTable.Id_drag_point_view);
        }
    }
}
