package com.javonlee.sample;

import com.javonlee.dragpointview.util.MathUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OhosTest {

    @Test
    public void testCalDistance() {
        assertEquals(0, (int) MathUtils.getDistance(new Point(25f, 25f), new Point(25f, 25f)) * 1000);
    }

    @Test
    public void testCalDistance2() {
        assertEquals(8000, (int) MathUtils.getDistance(new Point(20f, 18f), new Point(25f, 25f)) * 1000);
    }

    @Test
    public void testIntersection1() {
        Point[] p = MathUtils.getIntersectionPoints(0, 0, 12, 12d);
        assertEquals(2, p.length);
        assertEquals(0, p[0].getPointXToInt());
        assertEquals(11, p[0].getPointYToInt());
        assertEquals(0, p[1].getPointXToInt());
        assertEquals(-11, p[1].getPointYToInt());
    }

}