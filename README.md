# DragPointView

  方便好用的自定义组件， 可以快速的实现拖放未读信息， 还可以自定义效果。


#### 功能说明
功能演示

  ![](./images/dragPointView.gif)

#### 安装教程

1. 在build.gradle文件中添加依赖
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation 'com.gitee.archermind-ti:DragPointView:1.0.0-beta'
    ......
}
```


#### 使用说明
直接在XML中使用
```
            <com.javonlee.dragpointview.view.DragPointView
                ohos:id="$+id:drag_point_view1"
                ohos:height="20fp"
                ohos:width="20fp"
                ohos:background_element="$graphic:shape_drag_point_red"
                ohos:layout_alignment="right|top"
                ohos:right_margin="6vp"
                ohos:text="66"
                ohos:text_alignment="center"
                ohos:text_color="#fff"
                ohos:text_size="12fp"
                ohos:top_margin="3vp"
                app:centerMinRatio="0.5"
                app:clearSign="test"
                app:colorStretching="#FFFF0000"
                app:dragCircleRadius="100vp"
                app:maxDragLength="100vp"
                app:recoveryAnimBounce="0.25"/>
```
在代码中
```
DragPointView dragPointView = (DragPointView) findComponentById(ResourceTable.Id_drag_point_view);
pointView.setRemoveAnim(animationDrawable);
pointView.setOnPointDragListener(listener);
```

# Attribute
* maxDragLength:Within this range, the Bessel rendering section will be shown, with the default value of Math.min (w, h)*3.
* centerCircleRadius:The initial radius of the center circle.
* dragCircleRadius:The initial radius of the drag circle.
* centerMinRatio:When dragging, the center circle becomes smaller and smaller with distance, and the property controls its minimum coefficient range: 0.5f~1.0f.
* recoveryAnimDuration:If the stretch length is not up to the threshold, the animation will be recovery, specifying the length of the animation.
* recoveryAnimBounce:recovery animation bounce factor, range 0.0f~1.0f.
* colorStretching:Bessel painted some colors, recommended consistent with the widget background.
* sign:Mark the category of this control and use it with clearSign.
* clearSign:The clearSign specified widgets are removed at the same time as they are removed.
* canDrag:Controls whether or not to allow drag and drop.

# 编译说明
在IDE界面， 选择右侧Gradle, 依次选择DragPointView--ohos:release--packageReleaseHar， 就可以在工程目录的DragPointView\build\outputs\har\release找到对应的har文件.

# 未完成项


# 遗留问题
OHOS的ListContainer暂时没找到方法屏蔽拖动排序，因此无法拖动列表项目的时候， 列表项也会跟着一起动。

#### 版权和许可信息
```
Copyright 2017 javonlee

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

