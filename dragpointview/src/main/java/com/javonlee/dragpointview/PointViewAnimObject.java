package com.javonlee.dragpointview;

import com.javonlee.dragpointview.view.ClearViewHelper;
import com.javonlee.dragpointview.view.AbsDragPointView;
import ohos.agp.animation.*;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * Created by lijinfeng on 2017/7/24.
 */

public class PointViewAnimObject {

    private final Object mObject;
    private AbsDragPointView mView;
    private Element background;
    private final EventHandler mEventHandler;
    private boolean startByWindowView = true;

    public PointViewAnimObject setView(AbsDragPointView view) {
        this.mView = view;
        return this;
    }

    public PointViewAnimObject(Object object, AbsDragPointView view) {
        this.mObject = object;
        this.mView = view;
        mEventHandler = new EventHandler(EventRunner.getMainEventRunner());
    }

    public void start(OnPointDragListener removeListener, Component origView, int x, int y) {
        startByWindowView = true;
        if (mObject == null) {
            throw new MyRuntimeException("remove anim is null.");
        }
        if (removeListener != null) {
            removeListener.onRemoveStart(mView);
        }
        mView.setPivotX(mView.getWidth() / 2f);
        mView.setPivotY(mView.getHeight() / 2f);
        if (mObject instanceof FrameAnimationElement) {
            background = mView.getBackgroundElement();
            if (x > 0) {
                start((FrameAnimationElement) mObject, origView.getEstimatedWidth(), origView.getEstimatedHeight(), x, y, removeListener);
            } else {
                start((FrameAnimationElement) mObject, mView.getWidth(), mView.getHeight(), mView.getLocationOnScreen()[0], mView.getLocationOnScreen()[1], removeListener);
            }

        } else if (mObject instanceof Animator) {
            start((Animator) mObject, removeListener);
        }
    }

    private void start(FrameAnimationElement object, int width, int height, int x, int y, final OnPointDragListener removeListener) {
        int duration = 0;
        for (int i = 0; i < object.getNumberOfFrames(); i++) {
            duration += object.getDuration(i);
        }
        mEventHandler.postTask(() -> {
            mView.setBackground(background);
            end(removeListener);
        }, duration + 5);
        mView.setText("");
        int drawableL = (width + height) / 2;
        if (startByWindowView) {
            drawableL = (width + height) / 2;
        } else {
            drawableL = (mView.getWidth() + mView.getHeight()) / 2;
        }
        ComponentContainer.LayoutConfig lp = mView.getLayoutConfig();
        lp.height = drawableL;
        lp.width = drawableL;
        mView.setLayoutConfig(lp);
        if (startByWindowView) {
            mView.setTranslation(x, y);
        }
        mView.setBackground(object);
        if (isFrameAnimationElementRunning(object)) {
            object.stop();
        }
        object.start();
        startByWindowView = false;
    }

    static void addListener(Animator animator, Animator.StateChangedListener listener) {
        if (animator instanceof AnimatorValue) {
            ((AnimatorValue) animator).setStateChangedListener(listener);
        } else if (animator instanceof AnimatorGroup) {
            ((AnimatorGroup) animator).setStateChangedListener(listener);
        } else if (animator instanceof AnimatorProperty) {
            ((AnimatorProperty) animator).setStateChangedListener(listener);
        }
    }

    static void removeListener(Animator animator) {
        if (animator instanceof AnimatorValue) {
            ((AnimatorValue) animator).setStateChangedListener(null);
        } else if (animator instanceof AnimatorGroup) {
            ((AnimatorGroup) animator).setStateChangedListener(null);
        } else if (animator instanceof AnimatorProperty) {
            ((AnimatorProperty) animator).setStateChangedListener(null);
        }
    }

    private void start(Animator object, final OnPointDragListener removeListener) {
        mView.setVisibility(Component.VISIBLE);
        mView.setRemoveAnim(object);
        removeListener(object);
        addListener(object, new Animator.StateChangedListener() {
            public void onStart(Animator animator) {}
            public void onStop(Animator animator) {}
            public void onCancel(Animator animator) {}
            public void onEnd(Animator animator) {
                removeListener(animator);
                end(removeListener);
            }
            public void onPause(Animator animator) {}
            public void onResume(Animator animator) {}
        });
        object.start();
    }

    private void end(OnPointDragListener listener) {
        mView.setVisibility(Component.INVISIBLE);
        mView.reset();
        if (listener != null) {
            listener.onRemoveEnd(mView);
        }
        AbsDragPointView nextRemoveView = mView.getNextRemoveView();
        if (nextRemoveView != null) {
            mView.setNextRemoveView(null);
            nextRemoveView.startRemove();
        } else {
            ClearViewHelper.getInstance().clearSignOver(mView.getSign());
        }
    }

    public void cancel() {
        if (mObject == null) {
            throw new MyRuntimeException("remove anim is null.");
        }
        if (mObject instanceof FrameAnimationElement) {
            ((FrameAnimationElement) mObject).stop();
        } else if (mObject instanceof Animator) {
            ((Animator) mObject).cancel();
        }
    }

    public boolean isRunning() {
        if (mObject == null) {
            return false;
        }
        if (mObject instanceof FrameAnimationElement) {
            FrameAnimationElement frameAnimationElement = (FrameAnimationElement) mObject;
            isFrameAnimationElementRunning(frameAnimationElement);
        } else if (mObject instanceof Animator) {
            return ((Animator) mObject).isRunning();
        }
        return false;
    }

    private static boolean isFrameAnimationElementRunning(FrameAnimationElement frameAnimationElement) {
        return frameAnimationElement.getNumberOfFrames() < frameAnimationElement.getNumberOfFrames();
    }
}

class MyRuntimeException extends RuntimeException {
    public MyRuntimeException(String message){
        super(message);
    }
}