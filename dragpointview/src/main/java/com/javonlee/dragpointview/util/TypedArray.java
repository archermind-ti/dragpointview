/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.javonlee.dragpointview.util;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;

import java.util.Optional;

public class TypedArray {
    private AttrSet mAttrSet;

    public TypedArray(AttrSet attrs) {
        mAttrSet = attrs;
    }

    public int getInt(String id, int defaultValue) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        return o.map(Attr::getIntegerValue).orElse(defaultValue);
    }

    public Float getFloat(String id, float defaultValue) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        return o.map(Attr::getFloatValue).orElse(defaultValue);
    }

    public boolean getBoolean(String id, boolean defaultValue) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        return o.map(Attr::getBoolValue).orElse(defaultValue);
    }

    public int getColor(String id, int defaultValue) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        return o.map(Attr::getIntegerValue).orElse(defaultValue);
    }

    public String getString(String id, String defaultValue) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        if (o.isPresent()) {
            return o.get().getStringValue();
        }
        return defaultValue;
    }

    public String getString(String id) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        return o.map(Attr::getStringValue).orElse(null);
    }

    public int getVpSize(String id, int defaultVp) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        String s = o.map(Attr::getStringValue).orElse(defaultVp + "vp");
        if (s.endsWith("vp")) {
            s = s.substring(0, s.length() - 2);
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException e) {
                return defaultVp;
            }
        }
        return defaultVp;
    }

    public int getResourceId(String id, int defaultValue) {
        Optional<Attr> o = mAttrSet.getAttr(id);
        return o.map(Attr::getIntegerValue).orElse(defaultValue);
    }

    public void recycle() {
        mAttrSet = null;
    }
}
