/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.javonlee.dragpointview.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

final public class LogUtils {

    private static final String format = "%{public}s:%{public}s";

    private LogUtils() {

    }

    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00, "DragPointView");

    public static void w(String tag, String s) {
        HiLog.warn(label, format, tag, s);
    }

    public static void e(String tag, String s) {
        HiLog.error(label, format, tag, s);
    }

    public static void i(String tag, String s) {
        HiLog.info(label, format, tag, s);
    }

    public static void d(String tag, String s) {
        HiLog.debug(label, format, tag, s);
    }

    public static void v(String tag, String s) {
        HiLog.debug(label, format, tag, s);
    }
}
