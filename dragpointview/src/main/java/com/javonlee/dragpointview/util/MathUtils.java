package com.javonlee.dragpointview.util;

import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Point;
import ohos.app.Context;

/**
 * Created by Administrator on 2017/7/15.
 */
final public class MathUtils {

    private MathUtils() {
    }

    /**
     * Get the point of intersection between circle and line.
     *
     * @param radius The circle radius.
     * @param lineK  The slope of line which cross the pMiddle.
     * @return
     */
    public static Point[] getIntersectionPoints(float cx, float cy, float radius, Double lineK) {
        Point[] points = new Point[2];

        float radian, xOffset = 0, yOffset = 0;
        if (lineK != null) {

            radian = (float) Math.atan(lineK);
            xOffset = (float) (Math.cos(radian) * radius);
            yOffset = (float) (Math.sin(radian) * radius);
        } else {
            xOffset = radius;
            yOffset = 0;
        }
        points[0] = new Point(cx + xOffset, cy + yOffset);
        points[1] = new Point(cx - xOffset, cy - yOffset);

        return points;
    }

    /**
     * Gets the distance between two points.
     *
     * @param p1
     * @param p2
     * @return
     */
    public static double getDistance(Point p1, Point p2) {
        return Math.sqrt((p1.getPointX() - p2.getPointX()) * (p1.getPointX() - p2.getPointX()) + (p1.getPointY() - p2.getPointY()) * (p1.getPointY() - p2.getPointY()));
    }
}
