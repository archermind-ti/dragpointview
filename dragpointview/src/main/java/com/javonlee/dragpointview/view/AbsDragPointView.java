package com.javonlee.dragpointview.view;

import com.javonlee.dragpointview.OnPointDragListener;
import com.javonlee.dragpointview.PointViewAnimObject;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.app.Context;

/**
 * Created by lijinfeng on 2017/7/29.
 */

public abstract class AbsDragPointView extends Text {

    protected float mCenterRadius;
    protected float mDragRadius;
    protected float mCenterMinRatio;
    protected float mRecoveryAnimBounce;
    protected int mMaxDragLength;
    protected int mColorStretching;
    protected int mRecoveryAnimDuration;
    protected String mSign;
    protected String mClearSign;
    protected boolean mCanDrag;

    protected PointViewAnimObject mRemoveAnim;
    protected Animator.TimelineCurve mRecoveryAnimInterpolator;
    protected OnPointDragListener mOnPointDragListener;
    protected AbsDragPointView mNextRemoveView;

    public AbsDragPointView(Context context) {
        super(context);
    }

    public AbsDragPointView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public AbsDragPointView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PointViewAnimObject getRemoveAnim() {
        return mRemoveAnim;
    }

    public AbsDragPointView setRemoveAnim(PointViewAnimObject removeAnim) {
        this.mRemoveAnim = removeAnim;
        return this;
    }

    public AbsDragPointView setRemoveAnim(Animator removeAnim) {
        this.mRemoveAnim = new PointViewAnimObject(removeAnim, this);
        return this;
    }

    public AbsDragPointView setRemoveAnim(FrameAnimationElement removeAnim) {
        this.mRemoveAnim = new PointViewAnimObject(removeAnim, this);
        return this;
    }

    public OnPointDragListener getOnPointDragListener() {
        return mOnPointDragListener;
    }

    public String getClearSign() {
        return mClearSign;
    }

    public AbsDragPointView setClearSign(String clearSign) {
        this.mClearSign = clearSign;
        return this;
    }

    public float getCenterRadius() {
        return mCenterRadius;
    }

    public AbsDragPointView setCenterRadius(float centerRadius) {
        this.mCenterRadius = centerRadius;
        invalidate();
        return this;
    }

    public float getDragRadius() {
        return mDragRadius;
    }

    public AbsDragPointView setDragRadius(float dragRadius) {
        this.mDragRadius = dragRadius;
        invalidate();
        return this;
    }

    public int getMaxDragLength() {
        return mMaxDragLength;
    }

    public AbsDragPointView setMaxDragLength(int maxDragLength) {
        this.mMaxDragLength = maxDragLength;
        return this;
    }

    public float getCenterMinRatio() {
        return mCenterMinRatio;
    }

    public AbsDragPointView setCenterMinRatio(float centerMinRatio) {
        this.mCenterMinRatio = centerMinRatio;
        invalidate();
        return this;
    }

    public int getRecoveryAnimDuration() {
        return mRecoveryAnimDuration;
    }

    public AbsDragPointView setRecoveryAnimDuration(int recoveryAnimDuration) {
        this.mRecoveryAnimDuration = recoveryAnimDuration;
        return this;
    }

    public float getRecoveryAnimBounce() {
        return mRecoveryAnimBounce;
    }

    public AbsDragPointView setRecoveryAnimBounce(float recoveryAnimBounce) {
        this.mRecoveryAnimBounce = recoveryAnimBounce;
        return this;
    }

    public int getColorStretching() {
        return mColorStretching;
    }

    public AbsDragPointView setColorStretching(int colorStretching) {
        this.mColorStretching = colorStretching;
        invalidate();
        return this;
    }

    public String getSign() {
        return mSign;
    }

    public AbsDragPointView setSign(String sign) {
        this.mSign = sign;
        return this;
    }

    public void setRecoveryAnimInterpolator(Animator.TimelineCurve recoveryAnimInterpolator) {
        this.mRecoveryAnimInterpolator = recoveryAnimInterpolator;
    }

    public Animator.TimelineCurve getRecoveryAnimInterpolator() {
        return mRecoveryAnimInterpolator;
    }

    public void clearRemoveAnim() {
        this.mRemoveAnim = null;
    }

    public AbsDragPointView setOnPointDragListener(OnPointDragListener onDragListener) {
        this.mOnPointDragListener = onDragListener;
        return this;
    }

    public boolean isCanDrag() {
        return mCanDrag;
    }

    public AbsDragPointView setCanDrag(boolean canDrag) {
        this.mCanDrag = canDrag;
        return this;
    }

    public AbsDragPointView getNextRemoveView() {
        return mNextRemoveView;
    }

    public void setNextRemoveView(AbsDragPointView nextRemoveView) {
        this.mNextRemoveView = nextRemoveView;
    }

    public abstract void reset();

    public abstract void startRemove();


}
