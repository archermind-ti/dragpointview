package com.javonlee.dragpointview.view;

import com.javonlee.dragpointview.util.MathUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.TextAlignment;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by lijinfeng on 2017/7/15.
 */
class DragPointViewWindow extends AbsDragPointView implements AnimatorValue.ValueUpdateListener, Animator.StateChangedListener, Component.DrawTask,
        Component.LayoutRefreshedListener, Component.TouchEventListener, Component.BindStateChangedListener {

    private DragPointView origView;
    //    private PixelMap origBitmap;
    private Paint mPaint;
    private Paint mFontPaint;
    private Path mPath;
    private int mWidthHalf, mHeightHalf;
    private float mRatioRadius;
    private int mMaxRadiusTrebling;
    private boolean isInCircle;
    private float downX, downY;
    private Point[] mDragTangentPoint;
    private Point[] mCenterTangentPoint;
    private Point mCenterCircle;
    private Point mCenterCircleCopy;
    private Point mDragCircle;
    private Point mDragCircleCopy;
    private double mDistanceCircles;
    private Point mControlPoint;
    private boolean mIsDragOut;
    private AnimatorValue mRecoveryAnim;
    private float origX, origY;
    private int mFontSize;
    private Color mTextColor;
    private String mText;

    //    public void setOrigBitmap(PixelMap origBitmap) {
    //        this.origBitmap = origBitmap;
    //    }

    public void setOrigText(String text, Color textColor, int fontSize) {
        mText = text;
        mTextColor = textColor;
        mFontSize = fontSize;
        mFontPaint.setTextSize(mFontSize);
        mFontPaint.setColor(mTextColor);
        mFontPaint.setTextAlign(TextAlignment.CENTER);
    }

    public String getClearSign() {
        return mClearSign;
    }

    public DragPointViewWindow setClearSign(String clearSign) {
        this.mClearSign = clearSign;
        return this;
    }

    public DragPointViewWindow setCenterRadius(float centerRadius) {
        this.mCenterRadius = centerRadius;
        return this;
    }

    public DragPointViewWindow setDragRadius(float dragRadius) {
        this.mDragRadius = dragRadius;
        return this;
    }

    public DragPointViewWindow(Context context) {
        super(context);
        init();
    }

    public DragPointViewWindow(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public DragPointViewWindow(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public void onRefreshed(Component component) {
        initSize();
    }

    public void setMyPosition(float x, float y) {
        origX = x;
        origY = y;
    }

    private void initSize() {
        float x = origView.getEstimatedWidth() / 2f;
        float y = origView.getEstimatedHeight() / 2f;
        mCenterCircle.modify(origX + x, origY + y);
        mDragCircle.modify(origX + x, origY + y);
        mWidthHalf = (int) x;
        mHeightHalf = (int) y;
        int flowMaxRadius = Math.min(mWidthHalf, mHeightHalf);
        mMaxRadiusTrebling = flowMaxRadius * 3;
    }

    private void init() {
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setTextSize(18);
        mPaint.setColor(new Color(mColorStretching));
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);

        mFontPaint = new Paint();
        mFontPaint.setAntiAlias(true);

        mDragTangentPoint = new Point[2];
        mCenterTangentPoint = new Point[2];
        mControlPoint = new Point();
        mCenterCircle = new Point();
        mCenterCircleCopy = new Point();
        mDragCircle = new Point();
        mDragCircleCopy = new Point();

        addDrawTask(this);
        setLayoutRefreshedListener(this);
        setBindStateChangedListener(this);
        setTouchEventListener(this);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getBackgroundElement() != null) {
            return;
        }
        drawCenterCircle(canvas);
        if (isInCircle) {
            drawBezierLine(canvas);
            drawOriginBitmap(canvas);
        }
    }

    private void drawOriginBitmap(Canvas canvas) {

        canvas.drawCircle(mDragCircle.getPointX(), mDragCircle.getPointY(), mWidthHalf, mPaint);

        canvas.drawText(mFontPaint, mText, mDragCircle.getPointX(), mDragCircle.getPointY() + mFontSize / 3f);
    }

    private void drawCenterCircle(Canvas canvas) {
        if (mIsDragOut || !isInCircle) {
            return;
        }
        mPaint.setColor(new Color(mColorStretching));
        mRatioRadius = Math.min(mCenterRadius, Math.min(mWidthHalf, mHeightHalf));
        if (isInCircle && Math.abs(mCenterMinRatio) < 1.f) {
            mRatioRadius = (float) (Math.max((mMaxDragLength - mDistanceCircles) * 1.f / mMaxDragLength, Math.abs(mCenterMinRatio)) * mCenterRadius);
            mRatioRadius = Math.min(mRatioRadius, Math.min(mWidthHalf, mHeightHalf));
        }
        canvas.drawCircle(mCenterCircle.getPointX(), mCenterCircle.getPointY(), mRatioRadius, mPaint);
    }

    public void setOrigView(DragPointView origViewIn) {
        this.origView = origViewIn;
    }

    private void drawBezierLine(Canvas canvas) {
        if (mIsDragOut) {
            return;
        }
        mPaint.setColor(new Color(mColorStretching));
        float dx = mDragCircle.getPointX() - mCenterCircle.getPointX();
        float dy = mDragCircle.getPointY() - mCenterCircle.getPointY();
        // 控制点
        mControlPoint.modify((mDragCircle.getPointX() + mCenterCircle.getPointX()) / 2,
                (mDragCircle.getPointY() + mCenterCircle.getPointY()) / 2);

        // 四个切点
        if (dx != 0) {
            float k1 = dy / dx;
            float k2 = -1 / k1;
            mDragTangentPoint = MathUtils.getIntersectionPoints(
                    mDragCircle.getPointX(), mDragCircle.getPointY(), mDragRadius, (double) k2);
            mCenterTangentPoint = MathUtils.getIntersectionPoints(
                    mCenterCircle.getPointX(), mCenterCircle.getPointY(), mRatioRadius, (double) k2);
        } else {
            mDragTangentPoint = MathUtils.getIntersectionPoints(
                    mDragCircle.getPointX(), mDragCircle.getPointY(), mDragRadius, (double) 0);
            mCenterTangentPoint = MathUtils.getIntersectionPoints(
                    mCenterCircle.getPointX(), mCenterCircle.getPointY(), mRatioRadius, (double) 0);
        }
        // 路径构建
        mPath.reset();
        mPath.moveTo(mCenterTangentPoint[0].getPointX(), mCenterTangentPoint[0].getPointY());
        mPath.quadTo(mControlPoint.getPointX(), mControlPoint.getPointY(), mDragTangentPoint[0].getPointX(), mDragTangentPoint[0].getPointY());
        mPath.lineTo(mDragTangentPoint[1].getPointX(), mDragTangentPoint[1].getPointY());
        mPath.quadTo(mControlPoint.getPointX(), mControlPoint.getPointY(),
                mCenterTangentPoint[1].getPointX(), mCenterTangentPoint[1].getPointY());
        mPath.close();
        canvas.drawPath(mPath, mPaint);
    }

    private boolean preHandleTouchEvent () {
        if (!mCanDrag || ClearViewHelper.getInstance().isClearSigning(mSign)) {
            return false;
        }
        if ((mRecoveryAnim != null && mRecoveryAnim.isRunning())
                || (mRemoveAnim != null && mRemoveAnim.isRunning())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if(!preHandleTouchEvent()){
            return false;
        }

        if (mRecoveryAnim == null || !mRecoveryAnim.isRunning()) {
            switch (event.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    downX = event.getPointerScreenPosition(event.getIndex()).getX();
                    downY = event.getPointerScreenPosition(event.getIndex()).getY();
                    isInCircle = true;
                    invalidate();
                    break;
                case TouchEvent.POINT_MOVE:
                    float dx = (int) event.getPointerScreenPosition(event.getIndex()).getX() - downX;
                    float dy = (int) event.getPointerScreenPosition(event.getIndex()).getY() - downY;

                    mDragCircle.modify(origX + mWidthHalf + dx, origY + mWidthHalf + dy);
                    mDistanceCircles = MathUtils.getDistance(mCenterCircle, mDragCircle);
                    mIsDragOut = mIsDragOut ? mIsDragOut : mDistanceCircles > mMaxDragLength;
                    invalidate();
                    break;
                case TouchEvent.PRIMARY_POINT_UP:
                case TouchEvent.CANCEL:
                    // upX = event.getPointerScreenPosition(event.getIndex()).getX();
                    // upY = event.getPointerScreenPosition(event.getIndex()).getY();
                    upAndCancelEvent();
                    return false;
            }
        }
        return true;
    }

    private void upAndCancelEvent() {
        if (isInCircle && mDistanceCircles == 0) {
            reset();
            if (mOnPointDragListener != null) {
                mOnPointDragListener.onRecovery(this);
            }
        } else if (!mIsDragOut) {
            mCenterCircleCopy.modify(mCenterCircle.getPointX(), mCenterCircle.getPointY());
            mDragCircleCopy.modify(mDragCircle.getPointX(), mDragCircle.getPointY());
            if (mRecoveryAnim == null) {
                mRecoveryAnim = new AnimatorValue();
//                .ofFloat(1.f, -Math.abs(mRecoveryAnimBounce));
                mRecoveryAnim.setDuration(mRecoveryAnimDuration);
                mRecoveryAnim.setValueUpdateListener(this);
                mRecoveryAnim.setStateChangedListener(this);
            }
            if (mRecoveryAnimInterpolator != null) {
                mRecoveryAnim.setCurve(mRecoveryAnimInterpolator);
            }
            mRecoveryAnim.start();
        } else {
            if (mDistanceCircles <= mMaxRadiusTrebling) {
                reset();
                if (mOnPointDragListener != null) {
                    mOnPointDragListener.onRecovery(this);
                }
            } else if (!TextTool.isNullOrEmpty(mClearSign)) {
                ClearViewHelper.getInstance().clearPointViewBySign(origView, mClearSign);
            } else {
                startRemove();
            }
        }
    }

    @Override
    public void startRemove() {
        if (mRemoveAnim == null) {
            setVisibility(Component.HIDE);
            if (mNextRemoveView != null) {
                mNextRemoveView.startRemove();
            }
            if (mOnPointDragListener != null) {
                mOnPointDragListener.onRemoveStart(this);
                mOnPointDragListener.onRemoveEnd(this);
            }
        } else {
            mRemoveAnim.start(mOnPointDragListener, origView, (int) (mDragCircle.getPointX()), (int) (mDragCircle.getPointY()));
        }
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        initSize();
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        if (mRecoveryAnim != null && mRecoveryAnim.isRunning()) {
            mRecoveryAnim.cancel();
        }
        if (mRemoveAnim != null) {
            mRemoveAnim.cancel();
        }
    }

    @Override
    public void reset() {
        mIsDragOut = false;
        isInCircle = false;
        mDragCircle.modify(origX + mWidthHalf, origY + mHeightHalf);
        mCenterCircle.modify(origX + mWidthHalf, origY + mHeightHalf);
        mDistanceCircles = 0;
        invalidate();
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
    }


    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {

    }

    @Override
    public void onEnd(Animator animator) {
        reset();
        if (mOnPointDragListener != null) {
            mOnPointDragListener.onRecovery(this);
        }
    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }


}
