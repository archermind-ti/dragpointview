package com.javonlee.dragpointview.view;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.TextTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lijinfeng on 2017/7/24.
 */

final public class ClearViewHelper {

    private ClearViewHelper() {
    }

    public static ClearViewHelper getInstance() {
        return ClearViewHelperHelper.getClearViewHelper();
    }

    private Map<Integer, Boolean> clearSigning = new HashMap<>();

    private static Component getRootView(Component component) {
        if (component.getComponentParent() != null && component.getComponentParent() instanceof Component) {
            return getRootView((Component) component.getComponentParent());
        } else {
            return component;
        }
    }

    public void clearPointViewBySign(AbsDragPointView dragPointView, String clearSign) {
        List<AbsDragPointView> list = new ArrayList<>();
        list.add(dragPointView);
        getAllPointViewVisible(getRootView(dragPointView), list, clearSign); //TODO???
        list.remove(dragPointView);
        list.add(0, dragPointView);
        for (int i = 0; i < list.size() - 1; i++) {
            list.get(i).setNextRemoveView(list.get(i + 1));
        }
        clearSigning.put(clearSign.hashCode(), true);
        list.get(0).startRemove();
    }

    public void clearSignOver(String clearSign) {
        if (TextTool.isNullOrEmpty(clearSign)) {
            return;
        }
        clearSigning.put(clearSign.hashCode(), false);
    }

    public boolean isClearSigning(String clearSign) {
        if (TextTool.isNullOrEmpty(clearSign)) {
            return false;
        }
        Boolean clear = clearSigning.get(clearSign.hashCode());
        return clear == null ? false : clear.booleanValue();
    }

    private void getAllPointViewVisible(Component view, List<AbsDragPointView> list, String clearSign) {
        if (view instanceof ComponentContainer) {
            for (int i = 0; i < ((ComponentContainer) view).getChildCount(); i++) {
                Component child = ((ComponentContainer) view).getComponentAt(i);
                getAllPointViewVisible(child, list, clearSign);
            }
        } else if (view instanceof AbsDragPointView) {
            AbsDragPointView v = (AbsDragPointView) view;
            if (v.getVisibility() == Component.VISIBLE
                    && clearSign.equals(v.getSign())
                    && !list.contains(view)) {
                list.add((AbsDragPointView) view);
            }
        }
    }

    private static class ClearViewHelperHelper {
        final private static ClearViewHelper clearViewHelper = new ClearViewHelper();

        public static ClearViewHelper getClearViewHelper() {
            return clearViewHelper;
        }
    }

}
