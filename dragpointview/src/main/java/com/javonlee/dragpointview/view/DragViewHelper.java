package com.javonlee.dragpointview.view;

import com.javonlee.dragpointview.OnPointDragListener;
import com.javonlee.dragpointview.util.LogUtils;
import ohos.agp.components.*;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 原始View的ouTouch事件与自定义View关联
 * Created by lijinfeng on 2017/07/26.
 */
public class DragViewHelper implements Component.TouchEventListener, OnPointDragListener {

    private final Context mContext;
    private final DragPointView mOriginView;
    private DragPointViewWindow windowView;
    private OnPointDragListener onPointDragListener;
    private final Runnable animRunnable;

    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutConfig windowParams;
    private PositionLayout.LayoutConfig layoutParams;

    private final EventHandler handler;


    public void startRemove() {
        if (window == null) {
            addViewToWindow();
        }
        windowView.setNextRemoveView(mOriginView.getNextRemoveView());
        handler.postTask(animRunnable);
    }

    public DragViewHelper(Context context, final DragPointView originView) {
        this.mContext = context;
        this.mOriginView = originView;
        this.handler = new EventHandler(EventRunner.getMainEventRunner());
        this.mOriginView.setTouchEventListener(this);
        animRunnable = new Runnable() {
            @Override
            public void run() {
                windowView.startRemove();
            }
        };
    }

    private void initParams() {
        windowParams = new WindowManager.LayoutConfig();
        windowParams.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        windowParams.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
        windowParams.x = 0;
        windowParams.y = 0;
        layoutParams = new PositionLayout.LayoutConfig();
    }

    @Override
    public boolean onTouchEvent(Component v, TouchEvent event) {
        LogUtils.i("DragPointWindow", "onTouchEvent0 " + v);
        int action = event.getAction();
        if (action == TouchEvent.PRIMARY_POINT_DOWN) {
            ComponentParent parent = v.getComponentParent();
            if (parent == null) {
                LogUtils.i("DragPointWindow", "onTouchEvent parent is null");
                return false;
            }
            addViewToWindow();
        }
        return windowView.onTouchEvent(v, event);
    }

    public void addViewToWindow() {
        if (windowManager == null) {
            windowManager = WindowManager.getInstance();
        }
        if (windowView == null) {
            createWindowView();
        }
        if (windowParams == null ||
                layoutParams == null) {
            initParams();
        }

        int[] ps = mOriginView.getLocationOnScreen();

        layoutParams.width = PositionLayout.LayoutConfig.MATCH_PARENT;
        layoutParams.height = PositionLayout.LayoutConfig.MATCH_PARENT;
        windowView.setMyPosition(ps[0], ps[1] - 128);
        if (window == null) {
            windowView.setLayoutConfig(layoutParams);
        }

        windowView.setOrigView(mOriginView);
        windowView.setOrigText(mOriginView.getText(), mOriginView.getTextColor(), mOriginView.getTextSize());
        onPointDragListener = mOriginView.getOnPointDragListener();
        windowView.setVisibility(Component.VISIBLE);

        if (windowView.getComponentParent() != null) {
            windowView.getComponentParent().removeComponent(windowView);
        }

        PositionLayout stackLayout = new PositionLayout(mContext);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        stackLayout.setLayoutConfig(layoutConfig);
        stackLayout.setClipEnabled(false);
        stackLayout.addComponent(windowView);
        windowView.setClipEnabled(false);

        if (window != null) {
            try {
                windowManager.destroyWindow(window);
            } catch (Exception e) {
            }

            window = null;
        }
        window = windowManager.addComponent(stackLayout, mContext, WindowManager.LayoutConfig.MOD_APPLICATION);
        window.addWindowFlags(WindowManager.LayoutConfig.MOD_APPLICATION);
        window.addFlags(WindowManager.LayoutConfig.MOD_APPLICATION);
        window.setLayoutConfig(windowParams);
        window.setTransparent(true);

        mOriginView.setVisibility(Component.INVISIBLE);
    }

    private void createWindowView() {
        windowView = new DragPointViewWindow(mContext);
        windowView.setCanDrag(mOriginView.isCanDrag());
        windowView.setCenterMinRatio(mOriginView.getCenterMinRatio());
        windowView.setCenterRadius(mOriginView.getCenterRadius());
        windowView.setColorStretching(mOriginView.getColorStretching());
        windowView.setDragRadius(mOriginView.getDragRadius());
        windowView.setClearSign(mOriginView.getClearSign());
        windowView.setSign(mOriginView.getSign());
        windowView.setMaxDragLength(mOriginView.getMaxDragLength());
        windowView.setRecoveryAnimBounce(mOriginView.getRecoveryAnimBounce());
        windowView.setRecoveryAnimDuration(mOriginView.getRecoveryAnimDuration());
        windowView.setRecoveryAnimInterpolator(mOriginView.getRecoveryAnimInterpolator());
        if (mOriginView.getRemoveAnim() != null) {
            windowView.setRemoveAnim(mOriginView.getRemoveAnim().setView(windowView));
        }
        windowView.setOnPointDragListener(this);
    }

    @Override
    public void onRemoveStart(AbsDragPointView view) {
        if (onPointDragListener != null) {
            onPointDragListener.onRemoveStart(mOriginView);
        }
    }

    @Override
    public void onRemoveEnd(AbsDragPointView view) {
        if (windowManager != null && window != null) {
            try {
                windowManager.destroyWindow(window);
            } catch (Exception e) {
            }

            window = null;
        }
        if (onPointDragListener != null) {
            onPointDragListener.onRemoveEnd(mOriginView);
        }
        if (mOriginView != null) {
            mOriginView.setVisibility(Component.HIDE);
        }
    }

    @Override
    public void onRecovery(AbsDragPointView view) {
        if (windowManager != null && window != null) {
            try {
                windowManager.destroyWindow(window);
            } catch (Exception e) {
            }
            window = null;
        }
        if (mOriginView != null) {
            mOriginView.setVisibility(Component.VISIBLE);
        }
        if (onPointDragListener != null) {
            onPointDragListener.onRecovery(mOriginView);
        }
    }
}
