package com.javonlee.dragpointview.view;

import com.javonlee.dragpointview.util.TypedArray;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * Created by Administrator on 2017/7/15.
 */
public class DragPointView extends AbsDragPointView implements Component.LayoutRefreshedListener {

    public static final float DEFAULT_CENTER_MIN_RATIO = 0.5f;
    public static final int DEFAULT_RECOVERY_ANIM_DURATION = 200;

    private DragViewHelper dragViewHelper;

    public DragPointView(Context context) {
        super(context);
        init();
    }

    @Override
    public void onRefreshed(Component component) {
        int flowMaxRadius = Math.min(getEstimatedWidth() / 2, getEstimatedHeight() / 2);
        mCenterRadius = mCenterRadius == 0 ? flowMaxRadius : Math.min(mCenterRadius, flowMaxRadius);
        mDragRadius = mDragRadius == 0 ? flowMaxRadius : Math.min(mDragRadius, flowMaxRadius);
        mMaxDragLength = mMaxDragLength == 0 ? flowMaxRadius * 10 : mMaxDragLength;
    }

    public DragPointView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public DragPointView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = new TypedArray(attrs);
        mMaxDragLength = AttrHelper.vp2px(array.getVpSize("maxDragLength", 0), context);
        mCenterRadius = AttrHelper.vp2px(array.getVpSize("centerCircleRadius", 0), context);
        mDragRadius = AttrHelper.vp2px(array.getVpSize("centerCircleRadius", 0), context);
        mCenterMinRatio = array.getFloat("centerMinRatio", DEFAULT_CENTER_MIN_RATIO);
        mRecoveryAnimDuration = array.getInt("recoveryAnimDuration", DEFAULT_RECOVERY_ANIM_DURATION);
        mColorStretching = array.getColor("colorStretching", 0);
        mRecoveryAnimBounce = array.getFloat("recoveryAnimBounce", 0f);
        mSign = array.getString("sign");
        mClearSign = array.getString("clearSign");
        mCanDrag = array.getBoolean("canDrag", true);
        init();
    }

    @Override
    public void startRemove() {
        dragViewHelper.startRemove();
    }

    private void init() {
        dragViewHelper = new DragViewHelper(getContext(), this);
        setLayoutRefreshedListener(this);
    }

    @Override
    public void reset() {

    }


}
